<?php

define('APP_ROOT', dirname(__FILE__, 2));
define('URL_ROOT', 'YOUR_URL');
define('SITE_NAME', 'YOUR_SITE_NAME');

define('DB_HOST', 'localhost');
define('DB_USER', 'YOUR_USER_NAME');
define('DB_PASS', 'YOUR_PASSWORD');
define('DB_NAME', 'YOUR_DATABASE_NAME');
